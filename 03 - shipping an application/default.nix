{
    sources ? import ../nix/sources.nix,
    pkgs ? import sources.nixpkgs {}
}:
let
    app = pkgs.callPackage ./application.nix {};
    launcher = pkgs.callPackage ./launcher.nix {
        inherit app;
    };
    docker_image = pkgs.callPackage ./docker.nix {
        app_launcher = launcher;
    };
in
    docker_image

