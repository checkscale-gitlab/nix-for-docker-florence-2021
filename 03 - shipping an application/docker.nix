{ dockerTools, app_launcher, bash }:
dockerTools.buildLayeredImage {
  name = "gitops_greeter";
  tag = "latest";
  created = "now";
  config.Cmd = [ "${app_launcher}/bin/launch_app" ];
  config.ExposedPorts = {
      "8080/tcp" = {};
  };
  maxLayers = 10;
}