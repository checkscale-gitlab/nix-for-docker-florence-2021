{
    sources ? import ../nix/sources.nix,
    pkgs ? import sources.nixpkgs {}
}:
pkgs.dockerTools.buildLayeredImage {
  name = "nix_redis";
  tag = "latest";
  created = "now";
  maxLayers = 10;
  config.Cmd = [
      "${pkgs.redis}/bin/redis-server"
      "--bind 0.0.0.0"
  ];
  config.ExposedPorts = {
      "6379/tcp" = {};
  };
}