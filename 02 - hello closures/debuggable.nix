{
    sources ? import ../nix/sources.nix,
    pkgs ? import sources.nixpkgs {}
}:
pkgs.dockerTools.buildLayeredImage {
  name = "nix_redis";
  tag = "debuggable";
  created = "now";
  maxLayers = 10;
  config = {
    Cmd = [
        "${pkgs.redis}/bin/redis-server"
        "--bind 0.0.0.0"
    ];
    ExposedPorts = {
        "6379/tcp" = {};
    };
    Env = [
        "RANDOM_NUMBER=12"
    ];
  };
  contents = [
      pkgs.bash
      pkgs.coreutils
  ];
}