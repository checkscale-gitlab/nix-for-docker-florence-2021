# Size of the docker image and closures

What's in a docker image built with nix? There's exactly what you specify or what's referenced by it.

For example, if we say that the command should run let's say, `redis`, then the runtime dependencies of `redis` will be included in the image, and their dependencies too. This is called the `closure` of redis.

So if we want to debug our container by doing `docker exec -it magic_container  bash` we need to remember to add bash!
See redis.nix vs debuggable.nix

So instead of choosing a minimal FROM distro we choose to build FROM SCRATCH.

Need alpine because of musl libc? For simpler case you can use pkgsStatic and have everything built against musl, see minimal.nix that results in a hello world image that is less than 200k

## References

Optimization of docker images with layers: <https://grahamc.com/blog/nix-and-layered-docker-images>

Documentation of the docker tools for use with nix: <https://nixos.org/manual/nixpkgs/stable/#sec-pkgs-dockerTools>